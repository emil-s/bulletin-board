mod db;
mod image;
mod image_base64;
mod image_type;
mod routes;

use axum::{routing::get, Router};
use dotenv::dotenv;
use sqlx::mysql::MySqlPool;
use std::net::SocketAddr;

#[tokio::main]
async fn main() {
    dotenv().ok();

    let database_url = std::env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    let pool = MySqlPool::connect(&database_url)
        .await
        .expect("Failed to create database pool");

    let app = Router::new()
        .route("/images", get(routes::get_images).post(routes::add_image))
        .with_state(pool);

    let addr = SocketAddr::from(([127, 0, 0, 1], 3000));

    axum::Server::bind(&addr)
        .serve(app.into_make_service())
        .await
        .unwrap();
}
