use crate::{image_base64::ImageBase64, image_type::ImageType};
use base64::{engine::general_purpose, DecodeError, Engine as _};
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
pub struct Image {
    pub name: String,
    pub image_type: ImageType,
    pub bytes: Vec<u8>,
}

impl TryFrom<ImageBase64> for Image {
    type Error = DecodeError;

    fn try_from(value: ImageBase64) -> Result<Self, Self::Error> {
        Ok(Self {
            name: value.name,
            image_type: value.image_type,
            bytes: general_purpose::STANDARD.decode(value.bytes)?,
        })
    }
}
