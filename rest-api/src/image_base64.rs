use crate::{image::Image, image_type::ImageType};
use base64::{engine::general_purpose, Engine as _};
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
pub struct ImageBase64 {
    pub name: String,
    pub image_type: ImageType,
    pub bytes: String,
}

impl From<Image> for ImageBase64 {
    fn from(value: Image) -> Self {
        Self {
            name: value.name,
            image_type: value.image_type,
            bytes: general_purpose::STANDARD.encode(value.bytes),
        }
    }
}
