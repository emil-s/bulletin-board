use crate::image::Image;
use crate::image_type::ImageType;
use sqlx::mysql::{MySqlPool, MySqlQueryResult};

pub async fn fetch_images(pool: &MySqlPool) -> Result<Vec<Image>, sqlx::Error> {
    sqlx::query_as!(
        Image,
        "SELECT name, image_type as `image_type: ImageType`, bytes FROM images",
    )
    .fetch_all(pool)
    .await
}

pub async fn insert_image(pool: &MySqlPool, image: Image) -> Result<MySqlQueryResult, sqlx::Error> {
    sqlx::query!(
        "INSERT INTO images VALUES (?, ?, ?)",
        image.name,
        image.image_type,
        image.bytes
    )
    .execute(pool)
    .await
}
