use crate::db;
use crate::image_base64::ImageBase64;
use axum::http::StatusCode;
use axum::{extract::State, Json};
use sqlx::mysql::MySqlPool;

pub async fn get_images(
    State(pool): State<MySqlPool>,
) -> Result<Json<Vec<ImageBase64>>, StatusCode> {
    let Ok(result) = db::fetch_images(&pool).await else {
        return Err(StatusCode::INTERNAL_SERVER_ERROR);
    };

    match result.into_iter().map(|x| x.try_into()).collect() {
        Ok(as_base64) => Ok(Json(as_base64)),
        Err(_) => Err(StatusCode::INTERNAL_SERVER_ERROR),
    }
}

pub async fn add_image(
    State(pool): State<MySqlPool>,
    Json(payload): Json<ImageBase64>,
) -> StatusCode {
    let Ok(payload) = payload.try_into() else {
        return StatusCode::BAD_REQUEST;
    };

    match db::insert_image(&pool, payload).await {
        Ok(_) => StatusCode::CREATED,
        Err(_) => StatusCode::INTERNAL_SERVER_ERROR,
    }
}
