import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'bloc/image_bloc.dart';
import 'repository/image_repository.dart';
import 'screens/online_images_screen.dart';
import 'screens/offline_images_screen.dart';
import 'screens/take_picture_screen.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final cameras = await availableCameras();
  final firstCamera = cameras.first;

  runApp(MyApp(firstCamera));
}

class MyApp extends StatelessWidget {
  final CameraDescription camera;

  MyApp(this.camera);

  @override
  Widget build(BuildContext context) {
    final ImageRepository imageRepository = ImageRepository();

    return BlocProvider(
      create: (context) => ImageBloc(imageRepository),
      child: MaterialApp(
        home: HomeScreen(camera: camera),
      ),
    );
  }
}

class HomeScreen extends StatefulWidget {
  final CameraDescription camera;

  HomeScreen({required this.camera});

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int _currentIndex = 0;
  late List<Widget> _children;

  @override
  void initState() {
    super.initState();
    _children = [
      OnlineImagesScreen(),
      TakePictureScreen(camera: widget.camera),
      OfflineImagesScreen(),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: IndexedStack(
        index: _currentIndex,
        children: _children,
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentIndex,
        onTap: (index) {
          setState(() {
            _currentIndex = index;
          });
        },
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.cloud),
            label: 'Online Images',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.camera),
            label: 'Take a Picture',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.folder),
            label: 'Offline Images',
          ),
        ],
      ),
    );
  }
}
