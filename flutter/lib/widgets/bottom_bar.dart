import 'package:flutter/material.dart';

class BottomBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      items: const <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Icon(Icons.cloud),
          label: 'Online Images',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.camera),
          label: 'Take a Picture',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.folder),
          label: 'Offline Images',
        ),
      ],
      // Add navigation logic here
    );
  }
}
