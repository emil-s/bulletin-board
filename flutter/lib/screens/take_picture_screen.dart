import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:camera/camera.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:path_provider/path_provider.dart';
import '../bloc/image_bloc.dart';
import '../models/image_model.dart';

class TakePictureScreen extends StatefulWidget {
  final CameraDescription camera;

  const TakePictureScreen({
    Key? key,
    required this.camera,
  }) : super(key: key);

  @override
  TakePictureScreenState createState() => TakePictureScreenState();
}

class TakePictureScreenState extends State<TakePictureScreen> {
  late CameraController _controller;
  late Future<void> _initializeControllerFuture;

  @override
  void initState() {
    super.initState();
    _controller = CameraController(
      widget.camera,
      ResolutionPreset.medium,
    );
    _initializeControllerFuture = _controller.initialize();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder<void>(
        future: _initializeControllerFuture,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            return CameraPreview(_controller);
          } else {
            return Center(child: CircularProgressIndicator());
          }
        },
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.camera_alt),
        onPressed: () async {
          try {
            await _initializeControllerFuture;
            final XFile image = await _controller.takePicture();
            final bytes = await image.readAsBytes();
            final base64Image = base64Encode(bytes);

            // Save image locally
            final directory = await getApplicationDocumentsDirectory();
            final path = Directory('${directory.path}/images');
            if (!await path.exists()) {
              await path.create();
            }
            final File newImage =
                File('${path.path}/${DateTime.now().toIso8601String()}.jpg');
            await newImage.writeAsBytes(bytes);

            // Upload image to server
            final imageModel = ImageModel(
              name: 'new_image',
              imageType: 'jpg',
              bytes: base64Image,
            );

            BlocProvider.of<ImageBloc>(context).uploadImage(imageModel);
          } catch (e) {
            print('Exception caught while taking picture: $e');
          }
        },
      ),
    );
  }
}
