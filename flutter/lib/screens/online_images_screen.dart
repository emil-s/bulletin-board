import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../bloc/image_bloc.dart';
import '../models/image_model.dart';

class OnlineImagesScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final imageBloc = BlocProvider.of<ImageBloc>(context);
    imageBloc.fetchImages();

    return BlocBuilder<ImageBloc, ImageState>(
      builder: (context, state) {
        if (state is ImageLoading) {
          return CircularProgressIndicator();
        } else if (state is ImageLoaded) {
          return ListView.builder(
            itemCount: state.images.length,
            itemBuilder: (context, index) {
              final bytes = base64Decode(state.images[index].bytes);
              return Image.memory(bytes);
            },
          );
        } else if (state is ImageError) {
          return Text('Error: ${state.error}');
        }
        return SizedBox.shrink();
      },
    );
  }
}
