import 'dart:io';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';

class OfflineImagesScreen extends StatefulWidget {
  @override
  _OfflineImagesScreenState createState() => _OfflineImagesScreenState();
}

class _OfflineImagesScreenState extends State<OfflineImagesScreen> {
  List<File> _images = [];

  @override
  void initState() {
    super.initState();
    _loadImages();
  }

  _loadImages() async {
    final directory = await getApplicationDocumentsDirectory();
    final path = Directory('${directory.path}/images');

    if (await path.exists()) {
      final files = path.listSync();
      setState(() {
        _images = files.map((file) => File(file.path)).toList();
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: _images.length,
      itemBuilder: (context, index) {
        final bytes = _images[index].readAsBytesSync();
        return Image.memory(bytes);
      },
    );
  }
}
