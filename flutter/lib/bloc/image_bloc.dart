import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import '../models/image_model.dart';
import '../repository/image_repository.dart';

part 'image_state.dart';

class ImageBloc extends Cubit<ImageState> {
  final ImageRepository _imageRepository;

  ImageBloc(this._imageRepository) : super(ImageInitial());

  Future<void> fetchImages() async {
    try {
      emit(ImageLoading());
      final images = await _imageRepository.fetchImages();
      emit(ImageLoaded(images));
    } catch (e) {
      emit(ImageError(e.toString()));
    }
  }

  Future<void> uploadImage(ImageModel image) async {
    try {
      await _imageRepository.uploadImage(image);
      fetchImages();
    } catch (e) {
      emit(ImageError(e.toString()));
    }
  }
}
