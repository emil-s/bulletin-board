import 'dart:convert';
import 'package:http/http.dart' as http;
import '../models/image_model.dart';

class ImageRepository {
  Future<List<ImageModel>> fetchImages() async {
    final response = await http.get(Uri.parse('http://10.0.2.2:3000/images'));
    final List<dynamic> decodedJson = jsonDecode(response.body);
    return decodedJson
        .map((json) => ImageModel(
              name: json['name'],
              imageType: json['image_type'],
              bytes: json['bytes'],
            ))
        .toList();
  }

  Future<void> uploadImage(ImageModel image) async {
    try {
      final response = await http.post(
        Uri.parse('http://10.0.2.2:3000/images'),
        headers: {'Content-Type': 'application/json'},
        body: jsonEncode({
          'name': image.name,
          'image_type': image.imageType,
          'bytes': image.bytes,
        }),
      );
      if (response.statusCode != 201) {
        print('Error uploading image: ${response.body}');
        throw Exception('Failed to upload image');
      }
    } catch (e) {
      print('Exception caught while uploading image: $e');
      throw Exception('Failed to upload image');
    }
  }
}
