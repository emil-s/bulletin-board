class ImageModel {
  final String name;
  final String imageType;
  final String bytes;

  ImageModel(
      {required this.name, required this.imageType, required this.bytes});
}
